import 'dart:convert';

class Note {
  String noteName, noteIsDone;
  int noteId;
  Note({this.noteId, this.noteName, this.noteIsDone});

  factory Note.fromJson(Map<String, dynamic> item) {
    return Note(
      noteId: item['id'],
      noteName: item['name'].toString(),
      noteIsDone: item['is_done'].toString(),
    );
  }
  
  Map<String, dynamic> toJson() {
    return {"name":noteName};
  }

}

List<Note> profileFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return List<Note>.from(data.map((item) => Note.fromJson(item)));
}

String postToJson(Note data) {
    final dyn = data.toJson();
    return json.encode(dyn);
}