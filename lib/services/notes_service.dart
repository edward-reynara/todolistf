import 'dart:convert';
import 'package:todolistf/models/notes.dart';
import 'package:http/http.dart' as http;


class ApiService {
  final baseUrl = 'https://bc4cffc5.ngrok.io/todos';

  Future<List<Note>> getFromJson() async {
    final url = '$baseUrl/';
    final response = await http.get(url);

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((note) => new Note.fromJson(note)).toList();
    }
    else {
      throw Exception('Failed to load data from API');
    }
  }

  Future<bool> createPost(Note data) async {
    final response = await http.post('$baseUrl/store',
      body: postToJson(data),
      headers: {"content-type": "application/json"},
    );
    if (response.statusCode == 200) { return true; }
    else { return false; }
  }

  Future<bool> updateProfile(Note data) async {
    final response = await http.post(
      "$baseUrl/update/$data.noteId",
      headers: {"content-type": "application/json"},
      body: postToJson(data),
    );
    if (response.statusCode == 200) { return true; }
    else { return false; }
  }

  Future<bool> deleteProfile(int id) async {
    final response = await http.post(
      "$baseUrl/delete/$id",
      headers: {"content-type": "application/json"},
    );
    if (response.statusCode == 200) { return true; }
    else { return false; }
  }

}